package jpa;

import org.hsqldb.Server;
import org.hsqldb.persist.HsqlProperties;

public class HsqlDbServerMode {

    public static void main(String[] args) throws Exception {
        // jdbc:hsqldb:hsql://localhost:9001/db1
        HsqlProperties p = new HsqlProperties();
        p.setProperty("server.database.0", "mem:db1");
        p.setProperty("server.dbname.0", "db1");
        p.setProperty("server.port", "9001");
        Server server = new Server();
        server.setProperties(p);
        server.start();
    }

}
