package jpa;

import javax.persistence.*;

import org.junit.*;

public class Main {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("my-hsql-unit");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(new Person());

        em.getTransaction().commit();
        em.close();
        emf.close();
    }

}
