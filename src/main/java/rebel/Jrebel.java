package rebel;


public class Jrebel {

    public static void main(String[] args) throws InterruptedException {

        for (int i = 0; i < 1000000; i++) {
            Other.print(i);
            Thread.sleep(1000);
        }
    }
}

class Other {
    public static void print(int i) {
        System.out.println("-" + i);
    }
}

// java -javaagent:/opt/jrebel/jrebel.jar rebel.Jrebel