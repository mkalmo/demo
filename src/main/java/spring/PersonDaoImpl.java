package spring;

import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

//@Component
//@Profile("prod")
public class PersonDaoImpl implements PersonDao {

    public JdbcTemplate template;

    @Override
    public String getPersonName(Long id) {
        System.out.println("running: PersonDaoImpl.getPersonName()");

        return template.queryForObject(
                "select name from person where id = ?",
                new Object[] { id }, String.class);
    }

}
