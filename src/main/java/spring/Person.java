package spring;

import lombok.Data;

@Data
public class Person {
    private Integer id;
    private String name;
}
