package spring;

public interface PersonDao {
    String getPersonName(Long id);
}
