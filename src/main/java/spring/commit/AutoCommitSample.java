package spring.commit;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import spring.TestPersonDao;

import javax.sql.DataSource;

public class AutoCommitSample {

    public static void main(String[] args) {

        ConfigurableApplicationContext ctx =
                new AnnotationConfigApplicationContext(Conf.class);

        try (ctx) {

            DemoDao dao = ctx.getBean(DemoDao.class);

            dao.saveSomething();
            dao.readSomething();



        }
    }



}

