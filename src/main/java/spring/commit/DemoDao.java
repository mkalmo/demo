package spring.commit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class DemoDao {

    @Autowired
    public JdbcTemplate template;

    @Transactional
    public void saveSomething() {
        String sql = "insert into person (age) values (20)";

        template.execute(sql);
        template.execute(sql);
        template.execute(sql);
    }

    public void readSomething() {
        String sql = "select count(*) from person";

        template.execute(sql);
    }

}
