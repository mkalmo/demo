package debug;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class DebuggerExamples {

    @Test
    public void stepOverStepIn() {
        power(3, 2);
        int j = power(2, 30);
        System.out.println(j);
    }

    @Test
    public void stepReturn() {
        int j = power(2, 30);
        System.out.println(j);
    }

    @Test
    public void evaluateExpression() {

        Person person = new Person("Alice", 20);

        if (someCondition() && getPerson().equals(person)) {
            // do sth
        }

    }

    @Test
    public void printVsDebugger() {
        int value = 1;

        switch (value) {
            case 1:
                System.out.println("case 1");
            case 2:
                doSomething();
            default:
                doSomethingOther();
            }
    }

    @Test
    public void returnInsteadOfComment() {
        returnExample();
    }

    @Test
    public void callStack() { // huvitavatele kohtadele bookmark-id
        calculateSth();
    }

    @Test
    public void someoneAddsNull() {

        // ...

        calculateSomethingImportant();

        // ...

        StringBuilder sb = new StringBuilder();
        for (String each : list) {
            sb.append(each.toString());
        }
        System.out.println(sb.toString());

    }

    @Test
    public void tryCatch() {
        for (int i = 100; i >= 0; i--) {
            int j = i - 2;

            longAndComplicatedAndSometimesThrows(j, i, getPerson(i));
        }
    }

    private void addToList(String entry) {
        list.add(entry);
    }

    private void longAndComplicatedAndSometimesThrows(int x, int y, Person p) {
        // ...
        int z = 1 / x;
        // ...
    }

    @Test
    public void dropFrame() {
        dropFrameSub(2);
    }

    private void dropFrameSub(int i) {
        int j = power(i, 10);
        count++;
        System.out.println(j);
    }

    private int count = 0;

    private Person getPerson() {
        return new Person("Bob", 21);
    }

    private Person getPerson(int i) {
        return new Person("Bob", i);
    }

    private int power(int x, int power) {
        int result = 1;
        for (int i = 1; i <= power; i++) {
            result *= x;
        }
        return result;
    }

    private void calculateSth() {
        int i = 0;

        calculateSthSub(i);
    }

    private void calculateSthSub(int i) {
        int j = i + 5;

        calculateSthSubSub(j + 4);
    }

    private void calculateSomethingImportant() {
        addToList(item1);

        for (String item : items) {
            addToList(item);
        }

        addToList(item4);

        addToList(item3);
    }

    private void calculateSthSubSub(int i) {

        if (i == 9) {
            throw new IllegalStateException();
        }

    }

    private void returnExample() {

        saveData();

        // return;

        // .. a lot of code

        sendEmails();

        // .. a lot of code
    }

    private void saveData() {

    }

    private void sendEmails() {

    }

    private boolean someCondition() {
        return 1 < 3;
    }

    private void doSomething() {
    }

    private void doSomethingOther() {
    }

    List<String> list = new ArrayList<>();

    private class Person {

        private final String name;
        private final int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String toString() {
            return "Person [name=" + name + ", age=" + age + "]";
        }
    }

    String item1 = "Item 1";
    String item2 = "Item 2";
    String item3 = "Item 3";
    String item4 = null;

    List<String> items = Arrays.asList("Item 11", "Item 12", "Item 13", "Item 14", "Item 15", "Item 16");

}
