package debug;

import java.util.ArrayList;
import java.util.List;

public class BowlingCalculator {

    private List<Integer> list = new ArrayList<Integer>();

    public void hit(int pinCount) {
        list.add(pinCount);
    }

    public int getScore() {
        int total = 0;
        int i = 0;
        // input 1, 1, 5, 5, 3
        while (i < list.size()) {
            if (list.get(i) == 10) { // strike
                total += 10 + list.get(i + 1) + list.get(i + 2);
                i++;
            } else if (list.get(i) + list.get(i + 1) == 10) {
                // split
                total += 10 + list.get(i + 2);
                i += 2;
            } else {
                total += list.get(i) + list.get(i + 1);
                i += 2;
            }
        }

        return total;
    }

}
