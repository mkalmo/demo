package debug;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class RpnCalculatorTest {

    @Test
    public void evaluateDepthOneExpression() throws Exception {
        assertThat(new RpnCalculator().evaluate("1 3 6 + 2 * +"), is(19));
    }

}
