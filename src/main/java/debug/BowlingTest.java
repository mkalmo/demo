package debug;

import org.junit.Test;

public class BowlingTest {

    private BowlingCalculator c = new BowlingCalculator();

    @Test
    public void aNormalGame() throws Exception {
        play(1, 1, 5, 5, 3);

        System.out.println(c.getScore());
    }

    private void play(Integer ... hitCounts) {
        for (Integer hitCount : hitCounts) {
            c.hit(hitCount);
        }
    }

}
