package debug;

import java.util.Stack;

public class RpnCalculator {

    private Stack<Integer> stack = new Stack<>();

    // 1 3 6 + 2 * +
    public int evaluate(String string) {
        for (String elem : string.split(" ")) {
            if ("+".equals(elem)) {
                plus();
            } else if ("*".equals(elem)) {
                multiply();
            } else {
                enter();
                setAccumulator(Integer.parseInt(elem));
            }
        }

        return getAccumulator();
    }

    public void setAccumulator(int i) {
        if (stack.size() > 0) {
            stack.pop();
        }

        stack.push(i);
    }

    public int getAccumulator() {
        return stack.size() > 0 ? stack.peek() : 0;
    }

    public void multiply() {
        stack.push(stack.pop() * stack.pop());
    }

    public void plus() {
        stack.push(stack.pop() + stack.pop());
    }

    public void enter() {
        stack.push(getAccumulator());
    }
}