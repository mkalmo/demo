(function () {
    'use strict';

    var app = angular.module('app', ['ngRoute']);

    app.controller('PostCtrl', MyCtrl);

    function MyCtrl($http) {
        var vm = this;

        vm.title = 'Hello';
        vm.posts = [];
        vm.newTitle = '';
        vm.getPosts = getPosts;
        vm.addPost = addPost;

        getPosts();

        function addPost() {
            var newPost = {
                title: vm.newTitle
            };

            $http.post('/posts', newPost).then(function () {
                vm.getPosts();
            })
        }

        function getPosts() {
            $http.get('/posts').then(function (result) {
                vm.posts = result.data;
            });

        }

    }

})();