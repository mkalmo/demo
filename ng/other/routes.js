(function () {
    'use strict';

    angular.module('app').config(Conf);

    function Conf($routeProvider) {

        $routeProvider.when('/list', {
            templateUrl : 'other/list.html',
            controller : 'PostCtrl',
            controllerAs : 'vm'
        }).when('/add', {
            templateUrl : 'other/form.html',
            controller : 'PostCtrl',
            controllerAs : 'vm'
        }).otherwise('/list');

    }

})();