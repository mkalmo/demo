(function () {
    'use strict';

    var app = angular.module('app', []);

    app.controller('PostCtrl', Ctrl);

    function Ctrl($http, $log) {

        var vm = this;
        vm.insertNew = insertNew;
        vm.filter = filter;

        vm.newPost = {};
        vm.filterText = '';
        vm.posts = [];

        init();

        function init() {
            $http.get('api/posts').then(function (response) {
                vm.filterText = '';
                vm.posts = response.data;
            });
        }

        function filter() {
            var url = 'api/posts/search?key=' + vm.filterText;
            $log.debug('GET ' + url);
            $http.get(url).then(function (response) {
                vm.posts = response.data;
            }, errorHandler);
        }

        function insertNew() {
            var url = 'api/posts';
            $log.debug('POST ' + url);
            $log.debug('data: ' + JSON.stringify(vm.newPost));

            $http.post(url, vm.newPost).then(function () {
                vm.newPost = {};
                init();
            }, errorHandler);
        }

        function errorHandler(response) {
            if (response.data && response.data.errors) {
                $log.error('Errors: ' + JSON.stringify(response.data.errors));
            } else {
                $log.error('Error: ' + JSON.stringify(response));
            }
        }
    }


})();