package com.example;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<Post, Long> {

    List<Post> findByTitleContainingIgnoreCase(String title);

    List<Post> findByTitle(String title);

    @Query("SELECT p FROM Post p WHERE p.text LIKE %?1%")
    List<Post> findInText(String searchString);
}