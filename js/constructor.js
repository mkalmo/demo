function Post() {
    this.title = undefined;

    this.logTitle = function () {
        console.log('Title: ' + this.title);
    }
}

console.log(Object.keys(new Post())); // [ 'title', 'logTitle' ]

Post.prototype.logTitle = function () {
    console.log('Title1: ' + this.title);
}

var post = new Post();
post.title = 'Post 1';
post.logTitle(); // Title: Post 1
